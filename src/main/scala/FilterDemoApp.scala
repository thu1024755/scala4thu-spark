import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac042 on 2017/5/8.
  */
object FilterDemoApp extends App{
  val conf = new SparkConf().setAppName("FilterDemo")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  //odds
// sc.parallelize(1 to 100).filter(i=>i%2==1)
   // .foreach(v=>println(v))

  val char:Char='M'
  val str:String="M"

//firstLetterIsM

  sc.textFile("./name.txt")
    .filter(name=>name.startsWith("M"))
     // .filter(name=>name.head=='M')
    .foreach(println)




}
