package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac042 on 2017/5/15.
  */
object TopRatingsApp extends App{

  val conf = new SparkConf().setAppName("TopRatings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings:RDD[(Int,Double)]= sc.textFile("/Users/mac042/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(2)!="rating")
    //.filter(strs=>strs(1)!="movieId")
     .map(strs=>{
       //println(strs.mkString(","))
       (strs(1).toInt,strs(2).toDouble)
     })

  val totalRatingByMovieId = ratings.reduceByKey((acc,curr)=>acc+curr)
 totalRatingByMovieId.takeSample(false,5).foreach(println)

 // ratings.take(5).foreach(println)

val averageRatingByMovieId/*:RDD[(Int,(Double,Int))]*/=ratings.mapValues(v=>v ->1)
  .reduceByKey((acc,x)=>{
    (acc._1+x._1)->(acc._2+x._2)
}).mapValues(kv=>kv._1/kv._2.toDouble)



averageRatingByMovieId.takeSample(false,5).foreach(println)


  val top10=averageRatingByMovieId.sortBy(_._2,false).take(10)

top10.foreach(println)

  val movies: RDD[(Int,String)]=sc.textFile("/Users/mac042/Downloads/ml-20m/movies.csv")
      .map(str=>str.split(","))
    .filter(strs=>strs(0)!="movieId")
    .map(strs=>(strs(0).toInt,strs(1).toString))
movies.takeSample(false,5).foreach(println)

val joined:RDD[(Int,(Double,String))]=averageRatingByMovieId.join(movies)
  joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result")
  //joined.saveAsTextFile("result")
 // joined.takeSample(false,5).foreach(println)
sc.textFile("result").take(5).foreach(println)


}

