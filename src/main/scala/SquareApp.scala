import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac042 on 2017/5/1.
  */
object SquareApp extends App{

  val conf = new SparkConf().setAppName("Square")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)
  val intRDD=sc.parallelize(1 to 100000)
  val squares=intRDD.map(x=>x*x)
  squares.foreach(println)
  squares.collect().foreach(println)
  squares.take(10).foreach(println)




}
