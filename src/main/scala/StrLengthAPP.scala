import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac042 on 2017/5/1.
  */
object StrLengthAPP extends App{
  val conf = new SparkConf().setAppName("Strlength")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val nums=sc.textFile("nums.txt")
  val strLengthAPP: RDD[Int]=nums.map(_.length)
  //println(strLengthAPP.collect().toList)
  strLengthAPP.foreach(len=>println(len))





}
